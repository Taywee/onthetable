class_name Clickable
extends Area2D

signal clicked(object, button)

func _input_event(_viewport, event, _shape_idx):
    if event is InputEventMouseButton and event.pressed:
        emit_signal('clicked', self, event.button_index)
