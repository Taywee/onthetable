extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var music_player: AudioStreamPlayer3D = $MusicPlayer
onready var listener: Listener = $Listener
var holding = false
var held: Clickable
var held_position: Vector2
var held_object_original_position: Vector2
var dragging = false
var rotating = false

# Called when the node enters the scene tree for the first time.
func _ready():
    for child in get_children():
        if child is Clickable:
            update_clickable_audio(child)


func _on_Clickable_clicked(object: Clickable, button: int):
    print(button)
    if not holding:
        match button:
            1:
                holding = true
                dragging = true
                rotating = false
            2:
                holding = true
                rotating = true
                dragging = false

        if holding:
            held = object
            held_position = get_viewport().get_mouse_position()
            held_object_original_position = object.position

func _input(event):
    if held and event is InputEventMouseButton and not event.pressed:
        holding = false
        dragging = false
        rotating = false

func _process(_delta: float):
    if holding:
        var mouse_position: Vector2 = get_viewport().get_mouse_position()
        if dragging:
            print('moving ', held.name)
            held.position = held_object_original_position + mouse_position - held_position
        if rotating:
            print('held: ', held_position)
            print('mouse: ', mouse_position)
            var rotation_angle = held_position.angle_to_point(mouse_position)
            print('angle: ', rotation_angle)
            held.rotation = rotation_angle - PI / 2
        update_clickable_audio(held)

func update_clickable_audio(clickable: Clickable):
    var audio: Spatial = clickable.get_node('Audio')
    var new_audio_position: Vector2 = clickable.global_position / 24.0
    audio.translation = Vector3(
        new_audio_position.x,
        -1.6,
        -new_audio_position.y
        )
    var rotation = Quat(Vector3(0, 1, 0), clickable.rotation)
    audio.rotation = rotation.get_euler()
